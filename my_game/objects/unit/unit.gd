
#	The Name of the Top Node should be in
#	PascalCase and should represent the
#	use of the Object.
#
#	In this example, this is a Unit
#	Object. 
#
#	We have NOT renamed the
#	child Nodes for this Object. It
#	is more useful to know WHAT makes
#	up your Object, than to come up with 
#	Clever names to use.
#
#	Be mindful to rename your Nodes if you
#	choose to change the Type.


class_name Unit
extends KinematicBody2D

export(String) var world
export(int) var speed
export(Resource) var my_class
