
#	The Game object should contain everything
#	you need to manage your game.
#
#	It should include a Reference to all objects
#	in your game so that you can easily
#	instance() and new() them

extends Node

#	objects

const Unit = preload("res://my_game/objects/unit/unit.tscn")
const Player = preload("res://my_game/objects/player/player.tscn")
const Enemy = preload("res://my_game/objects/enemy/enemy.tscn")


#	variables

onready var score = 0
onready var lives = 0
