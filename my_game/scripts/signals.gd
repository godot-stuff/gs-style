
#	Signals should be kept in a 
#	seperate script and added in
#	the Autoload.
#
#	This will make them accessible
#	from any script in the game and
#	provides a central location
#	for connecting to.

extends Node

#	game signals

signal game_is_ending()
signal game_has_ended()
signal game_is_starting()
signal game_has_started()
signal game_start()
signal game_end()

#	score signals

signal score_is_changing(score)
signal score_has_changed(score)
