
"""

This Scene gives an example of how
to use the Signals Autoload in 
you Game.

Signals can be connected at any
time in the Game.

"""

extends Node

func _start_game():
	print("** Game Has Started **")
	

func _init():
	signals.connect("game_start", self, "_start_game")
	
	
func _ready():
	signals.emit_signal("game_start")
